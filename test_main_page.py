from .pages.main_page import MainPage
from .pages.login_page import LoginPage
from .pages.backet_page import BasketPage
import pytest


# pytest -v --tb=line -m login_guest --language=en
@pytest.mark.login_guest
class TestLoginFromMainPage:

    def test_guest_can_go_to_login_page(self, browser):
        link = "http://selenium1py.pythonanywhere.com/"
        page = MainPage(browser,
                        link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
        page.open()  # открываем страницу
        page.go_to_login_page()  # выполняем метод Base страницы — переходим на страницу логина
        login_page = LoginPage(browser, browser.current_url)  # Инициализируем LoginPage в теле теста
        login_page.should_be_login_page()  # Проверяем, что находимся на необходимой странице

    def test_guest_cant_see_product_in_basket_opened_from_main_page(self, browser):
        link = "http://selenium1py.pythonanywhere.com/"
        page = MainPage(browser,
                        link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
        page.open()  # открываем страницу
        page.go_to_basket_page()  # выполняем метод Base страницы — переходим на страницу корзины
        basket_page = BasketPage(browser, browser.current_url)  # Объявляем страницу корзины
        basket_page.should_be_basket_url()  # Проеверяем, что действительно находимся на странице корзины
        basket_page.should_be_empty_basket()  # Ожидаем, что в корзине нет товаров + есть соответствющее сообщение
