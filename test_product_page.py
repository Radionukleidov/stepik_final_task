from .pages.product_page import ProductPage
from .pages.login_page import LoginPage
import pytest
from .pages.base_page import BasePage
from .pages.backet_page import BasketPage


# pytest -s -v -rX test_product_page.py

@pytest.mark.skip(reason="Failed test - success message is present")
def test_guest_cant_see_success_message_after_adding_product_to_basket(browser):
    link = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"
    page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
    page.open()  # Открываем страницу товара
    page.add_product_to_basket()  # Добавляем товар в корзину
    page.should_not_be_success_message()  # Проверяем, что нет сообщения об успехе с помощью is_not_element_present


@pytest.mark.skip(reason="Failed test - success message is present")
def test_message_disappeared_after_adding_product_to_basket(browser):
    link = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"
    page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
    page.open()  # Открываем страницу товара
    page.add_product_to_basket()  # Добавляем товар в корзину
    page.should_disappear_success_message()  # Проверяем, что нет сообщения об успехе с помощью is_disappeared


def test_guest_should_see_login_link_on_product_page(browser):
    link = "http://selenium1py.pythonanywhere.com/en-gb/catalogue/the-city-and-the-stars_95/"
    page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
    page.open()  # Открываем страницу товара
    page.should_be_login_link()  # проверяем что доступна урла логина

@pytest.mark.need_review
def test_guest_can_go_to_login_page_from_product_page(browser):
    link = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"
    page = ProductPage(browser,
                       link)  # Инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()  # Открываем страницу
    page.go_to_login_page()  # Выполняем метод страницы — переходим на страницу логина
    login_page = LoginPage(browser, browser.current_url)  # Инициализируем LoginPage в теле теста
    login_page.should_be_login_page()  # Проверяем, что находимся на необходимой странице

@pytest.mark.need_review
def test_guest_cant_see_product_in_basket_opened_from_product_page(browser):
    link = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"
    page = ProductPage(browser,
                       link)  # Инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()  # Открываем страницу
    page.go_to_basket_page()  # выполняем метод Base страницы — переходим на страницу корзины
    basket_page = BasketPage(browser, browser.current_url)  # Объявляем страницу корзины
    basket_page.should_be_basket_url()  # Проеверяем, что действительно находимся на странице корзины
    basket_page.should_be_empty_basket()  # Ожидаем, что в корзине нет товаров + есть соответствющее сообщение


def test_guest_cant_see_success_message(browser):
    link = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"
    page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
    page.open()  # Открываем страницу товара
    page.should_not_be_success_message()  # Проверяем, что нет сообщения об успехе с помощью is_not_element_present

@pytest.mark.need_review
@pytest.mark.parametrize('links', [pytest.param(i, marks=pytest.mark.xfail(i == 7, reason='Bug in the page'))
                                   for i in range(10)])  # помечаем 7 тест как бажный
def test_guest_can_add_product_to_basket(browser, links):
    link = f"http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/?promo=offer{links}"
    page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
    page.open()  # Открываем страницу товара
    page.check_adding_product_to_basket()  # проверяем добавление продукта


class TestUserAddToBasketFromProductPage:
    @pytest.fixture(scope="function", autouse=True)
    def setup(self, browser):
        link = 'http://selenium1py.pythonanywhere.com/en-gb/accounts/login/'
        login_page = LoginPage(browser, link)
        login_page.open()
        login_page.should_be_login_page()
        login_page.register_new_user(BasePage.registration_email(), BasePage.registration_password())
        login_page.should_be_authorized_user()

    def test_user_cant_see_success_message(self, browser):
        link = "http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/"
        page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
        page.open()  # Открываем страницу товара
        page.should_not_be_success_message()  # Проверяем, что нет сообщения об успехе с помощью is_not_element_present

    @pytest.mark.need_review
    @pytest.mark.parametrize('links', [pytest.param(i, marks=pytest.mark.xfail(i == 7, reason='Bug in the page'))
                                       for i in range(10)])  # помечаем 7 тест как бажный
    def test_user_can_add_product_to_basket(self, browser, links):
        link = f"http://selenium1py.pythonanywhere.com/catalogue/coders-at-work_207/?promo=offer{links}"
        page = ProductPage(browser, link)  # инициализируем Page Object, передаем в конструктор драйвер и url адрес
        page.open()  # Открываем страницу товара
        page.check_adding_product_to_basket()  # проверяем добавление продукта


