from selenium.webdriver.common.by import By


class BasePageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")
    LOGIN_LINK_INVALID = (By.CSS_SELECTOR, "#login_link_inc")
    BTN_BASKET = (By.CSS_SELECTOR, '.btn-group .btn-default:nth-child(1)')
    USER_ICON = (By.CSS_SELECTOR, ".icon-user")


class BasketPageLocators:
    BASKET_ITEMS = (By.CSS_SELECTOR, '.basket-items')
    BASKET_EMPTY_TEXT = (By.CSS_SELECTOR, 'div#content_inner p')


class LoginPageLocators:
    BTN_REGISTER = (By.CSS_SELECTOR, "[class='col-sm-6 register_form'] .btn-primary")
    LOGIN_FORM = (By.CSS_SELECTOR, '#login_form')
    REGISTER_FORM = (By.CSS_SELECTOR, '#register_form')
    REGISTRATION_EMAIL = (By.CSS_SELECTOR, '#id_registration-email')
    REGISTRATION_PASSWORD_1 = (By.CSS_SELECTOR, '#id_registration-password1')
    REGISTRATION_PASSWORD_2 = (By.CSS_SELECTOR, '#id_registration-password2')
    SUCCESS_MESSAGE_AFTER_REGISTRATION = (By.CSS_SELECTOR, '#messages div:nth-of-type(1) .alertinner')


class MainPageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, "#login_link")


class ProductPageLocators:
    BTN_ADD_TO_BASKET = (By.CSS_SELECTOR, '.btn.btn-add-to-basket.btn-lg.btn-primary')
    PRODUCT_NAME_BEFORE_ADDING = (By.CSS_SELECTOR, '.col-sm-6.product_main h1')
    PRODUCT_PRICE = (By.CSS_SELECTOR, '.col-sm-6.product_main .price_color')
    PRODUCT_NAME_AFTER_ADDING = (By.CSS_SELECTOR, '#messages div:nth-child(1) div strong')
    CHECK_PROMO_CONDITIONS_MEETS = (By.CSS_SELECTOR, '#messages div:nth-child(2) div strong')
    PRODUCT_PRICE_AFTER_ADDING = (By.CSS_SELECTOR, '#messages div:nth-child(3) div strong')
    SUCCESS_MESSAGE = (
        By.CSS_SELECTOR, '#messages div:nth-of-type(1) .alertinner')  # all success message include product_name
