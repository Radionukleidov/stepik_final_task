from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import math
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from .locators import BasePageLocators
import random
import string


class BasePage:

    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)

    # Переход на страницу корзины
    def go_to_basket_page(self):
        self.should_be_btn_add_to_basket()
        btn_basket = self.browser.find_element(*BasePageLocators.BTN_BASKET)
        btn_basket.click()

    # Переход на страницу Логина
    def go_to_login_page(self):
        self.should_be_login_link()
        link = self.browser.find_element(*BasePageLocators.LOGIN_LINK)
        link.click()

    # Для перехватыватов исключений. В него будем передавать два аргумента: как искать (css, id, xpath и тд) и
    # собственно что искать (строку-селектор)
    def is_element_present(self, how, what):
        try:
            self.browser.find_element(how, what)
        except NoSuchElementException:
            return False
        return True

    # Проверяет, что элемент не появляется на странице в течение заданного времени
    # is_not_element_present: упадет, как только увидит искомый элемент. Не появился: успех, тест зеленый
    def is_not_element_present(self, how, what, timeout=4):
        try:
            WebDriverWait(self.browser, timeout).until(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return True
        return False

    # Проверка, что какой-то элемент исчезает (Будет ждать до тех пор, пока элемент не исчезнет)
    def is_disappeared(self, how, what, timeout=4):
        try:
            WebDriverWait(self.browser, timeout, 1, TimeoutException). \
                until_not(EC.presence_of_element_located((how, what)))
        except TimeoutException:
            return False
        return True

    # Для перехвата наличия алертов
    def is_alert_present(self):
        try:
            self.browser.switch_to.alert
        except NoAlertPresentException:
            return False
        return True

    # Открытие урла
    def open(self):
        self.browser.get(self.url)

    # Генерация рандомного емейлв
    @staticmethod
    def registration_email():
        rand_email = "autotestqa+" + str(random.randint(100, 9000000)) + "@testdata.com"
        return rand_email

    # Генерация рандомного пароля
    @staticmethod
    def registration_password():
        password = "".join(random.choice(string.ascii_letters) for i in range(random.randint(10, 15)))
        return password

    # првоерка наличия перехода на логин
    def should_be_login_link(self):
        assert self.is_element_present(
            *BasePageLocators.LOGIN_LINK), "Login link is not presented"  # модифицируем метод проверки ссылки на логин так, чтобы он выдавал адекватное сообщение об ошибке

    # Модифицируем метод проверки наличия кнопки перехода в корзину
    def should_be_btn_add_to_basket(self):
        assert self.is_element_present(*BasePageLocators.BTN_BASKET), "Button [View basket] is not presented"

    # Проверка того, что пользователь залогинен
    def should_be_authorized_user(self):
        assert self.is_element_present(*BasePageLocators.USER_ICON), "User icon is not presented," \
                                                                     " probably unauthorised user"

    # Для решения математической задачки в аллерте при добавлении в корзину товара
    def solve_quiz_and_get_code(self):
        alert = self.browser.switch_to.alert
        x = alert.text.split(" ")[2]
        answer = str(math.log(abs((12 * math.sin(float(x))))))
        alert.send_keys(answer)
        alert.accept()
        try:
            alert = self.browser.switch_to.alert
            alert_text = alert.text
            print(f"Your code: {alert_text}")
            alert.accept()
        except NoAlertPresentException:
            print("No second alert presented")
