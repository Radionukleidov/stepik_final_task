from .base_page import BasePage
from .locators import LoginPageLocators


class LoginPage(BasePage):
    # Подтверждаем введенный емейл
    def confirm_password_for_registration(self, password):
        fld_confirm_password = self.browser.find_element(*LoginPageLocators.REGISTRATION_PASSWORD_2)
        fld_confirm_password.send_keys(password)
        return password

    # Проверяем сообщение об успешной регистрации
    def check_success_registration_message(self):
        self.should_be_success_message_after_registration()
        message = self.browser.find_element(*LoginPageLocators.SUCCESS_MESSAGE_AFTER_REGISTRATION)
        text = message.text
        assert 'Thanks for registering!' in text, \
            "There is no 'Thanks for registering!' text in the success message!"
        return text

    # Вводим емейл для регистрации
    def fill_email_for_registration(self, email):
        fld_email = self.browser.find_element(*LoginPageLocators.REGISTRATION_EMAIL)
        fld_email.send_keys(email)
        return email

    # Вводим пароль для регистрации
    def fill_password_for_registration(self, password):
        fld_password = self.browser.find_element(*LoginPageLocators.REGISTRATION_PASSWORD_1)
        fld_password.send_keys(password)
        return password

    # Кликаем на кнопку регистрации после заполнения полей
    def register(self):
        btn_register = self.browser.find_element(*LoginPageLocators.BTN_REGISTER)
        btn_register.click()

    # Полный метод регистрации нового пользователя
    def register_new_user(self, email, password):
        self.fill_email_for_registration(email)
        self.fill_password_for_registration(password)
        self.confirm_password_for_registration(password)
        self.register()
        self.check_success_registration_message()

    # Проверка страницы логина
    def should_be_login_page(self):
        self.should_be_login_url()
        self.should_be_login_form()
        self.should_be_register_form()

    # Проверка урлы логина
    def should_be_login_url(self):
        current_url = self.browser.current_url
        assert 'accounts/login/' in current_url, \
            "Current url isn't 'http://selenium1py.pythonanywhere.com/en-gb/accounts/login/'"

    # Проверка наличия формы авторизации
    def should_be_login_form(self):
        assert self.is_element_present(*LoginPageLocators.LOGIN_FORM), "Login form is not presented"

    # Проверка наличия формы регистрации
    def should_be_register_form(self):
        assert self.is_element_present(*LoginPageLocators.REGISTER_FORM), "Register form is not presented"

    # Проверка наличия сообщения об успешной регистрации
    def should_be_success_message_after_registration(self):
        assert self.is_element_present(
            *LoginPageLocators.SUCCESS_MESSAGE_AFTER_REGISTRATION), "Success message after registration isn't presented"
