from .base_page import BasePage
from .locators import BasketPageLocators


class BasketPage(BasePage):
    # Проеверяем, что действительно находимся на странице корзины
    def should_be_basket_url(self):
        current_url = self.browser.current_url
        assert 'basket' in current_url, \
            "Current url isn't basket page"

    # Ожидаем, что в корзине нет товаров + есть соответствющее сообщение
    def should_be_empty_basket(self):
        self.should_not_be_any_added_products()
        self.check_empty_basket_text()

    # Ожидаем, что в корзине нет добавленных товаров
    def should_not_be_any_added_products(self):
        assert self.is_not_element_present(*BasketPageLocators.BASKET_ITEMS), "Basket items, but should not be"

    # Ожидаем, что есть блок в котором должен быть текст о том, что корзина пуста
    def should_be_block_about_empty_basket(self):
        assert self.is_element_present(
            *BasketPageLocators.BASKET_EMPTY_TEXT), "Message about empty basket is not presented"

    # Проверка того, что путсой корзине есть 'Your basket is empty.'
    def check_empty_basket_text(self):
        self.should_be_block_about_empty_basket()
        message = self.browser.find_element(*BasketPageLocators.BASKET_EMPTY_TEXT)
        text = message.text
        assert 'Your basket is empty' in text, "There is no 'Your basket is empty' text in the empty basket!"
        return text
