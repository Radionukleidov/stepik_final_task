from .base_page import BasePage
from .locators import ProductPageLocators


class ProductPage(BasePage):
    # Добавить в корзину
    def add_product_to_basket(self):
        btn = self.browser.find_element(*ProductPageLocators.BTN_ADD_TO_BASKET)
        btn.click()

    # Проверка добавления продукат в корзину
    def check_adding_product_to_basket(self):
        self.should_be_promo_new_year_url()
        self.should_be_btn_add_to_basket()
        self.get_product_name_before_adding()
        self.get_product_price_before_adding()
        self.add_product_to_basket()
        self.should_be_alert()
        self.solve_quiz_and_get_code()
        self.check_name_of_product_which_added()
        self.check_price_of_product_which_added()
        self.check_promo_conditions_meets()

    # Проверка названия добавленного товара на корректность
    def check_name_of_product_which_added(self):
        product_name_before_adding = self.get_product_name_before_adding()
        product_name_after_adding = self.get_product_name_after_adding()
        assert product_name_before_adding == product_name_after_adding, \
            f'Название продукта до добавления в корзину ({product_name_before_adding}) ' \
            f'не соответствует названию после добавления в корзину ({product_name_after_adding})!'

    # Проверка цены добавленного товара на корректность
    def check_price_of_product_which_added(self):
        product_price_before_adding = self.get_product_price_before_adding()
        product_price_after_adding = self.get_product_price_after_adding()
        assert product_price_before_adding == product_price_after_adding, \
            f'Цена продукта до добавления в корзину ({product_price_before_adding}) ' \
            f'не соответствует цене после добавления в корзину ({product_price_after_adding})!'

    # Проверка попадания в промоакцию
    def check_promo_conditions_meets(self):
        promo_conditions = len(self.browser.find_elements(*ProductPageLocators.CHECK_PROMO_CONDITIONS_MEETS))
        assert promo_conditions == 1, f'Count of meets promo conditions - {promo_conditions}, but expexted 1'

    # Проверка того, что в сообщении о добавлении в корзину есть 'has been added to your basket.'
    def check_success_message(self):
        self.should_be_success_message_block()
        message = self.browser.find_element(*ProductPageLocators.SUCCESS_MESSAGE)
        text = message.text
        assert 'has been added to your basket.' in text, \
            "There is no 'has been added to your basket.' text in the success message!"
        return text

    # Получение названия продукта после добавления в корзину
    def get_product_name_after_adding(self):
        product_name_after = self.browser.find_element(*ProductPageLocators.PRODUCT_NAME_AFTER_ADDING).text
        return product_name_after

    # Получение названия продукта до добавления в корзину
    def get_product_name_before_adding(self):
        product_name = self.browser.find_element(*ProductPageLocators.PRODUCT_NAME_BEFORE_ADDING).text
        return product_name

    # Получение цены продукта после добавления в корзину
    def get_product_price_after_adding(self):
        product_price_after = self.browser.find_element(*ProductPageLocators.PRODUCT_PRICE_AFTER_ADDING).text
        return product_price_after

    # Получение цены продукта до добавления в корзину
    def get_product_price_before_adding(self):
        product_price = self.browser.find_element(*ProductPageLocators.PRODUCT_PRICE).text
        return product_price

    # Проверка наличия в урле ?promo=newYear'
    def should_be_promo_new_year_url(self):
        current_url = self.browser.current_url
        url = self.url
        assert '?promo=' in current_url, "There is no '?promo=' in the current link!"
        return url

    # Проверка появления алерта
    def should_be_alert(self):
        assert self.is_alert_present(), 'Attention! Any alert no presented!'

    # Модифицируем метод проверки наличия кнопки добавления в корзину
    def should_be_btn_add_to_basket(self):
        assert self.is_element_present(*ProductPageLocators.BTN_ADD_TO_BASKET), "Login link is not presented"

    # Проверяем, что нет сообщения об успехе с помощью is_not_element_present
    # со страницы про отрицательные проверки https://stepik.org/lesson/201964/step/5?unit=176022
    # is_not_element_present: упадет, как только увидит искомый элемент. Не появился: успех, тест зеленый.
    def should_not_be_success_message(self):
        assert self.is_not_element_present(*ProductPageLocators.SUCCESS_MESSAGE), \
            "Success message is presented, but should not be"

    # Проверяем, что нет сообщения об успехе с помощью is_disappeared
    # со страницы про отрицательные проверки https://stepik.org/lesson/201964/step/5?unit=176022
    # is_disappeared: будет ждать до тех пор, пока элемент не исчезнет
    def should_disappear_success_message(self):
        assert self.is_disappeared(*ProductPageLocators.SUCCESS_MESSAGE), \
            "Success message still is presented, and not is disappear"

    #  проверка наявности блока в котором находится сообщение об успешном добавлени в корзину
    def should_be_success_message_block(self):
        assert self.is_element_present(*ProductPageLocators.SUCCESS_MESSAGE), "Success message is not presented"
